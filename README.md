# R Ladies February meeting: Data Visualization
## 28 February 2019
## Presented by Geraldine Klarenberg, PhD
### gklarenberg [at] ufl [dot] edu

This Gitlab repo contains the presentation, and the R scripts we discussed.

The folder "line_plot" contains a script to make a fairly straighforward line plot with the help of the package ggplot2.
There is some playing around with dates in there. The pdf "annual_precip" is the full figure that was developed at some point, 
from which the script is derived. In this script we only make one of those plots, "precip_lineplot".

The folder "radar_plot" shows how to make radar / spider plots, with a package that adds onto ggplot2. It also contains
code to turn all the small plots into one figure, and how to deal with a shared legend.
The pdf "cropinfo_spiderplots_example_full" is the original, large figure. In this script we develop a figure consisting of
6 little plots (to save time), "cropinfo_spiderplots"

The folder "network_plot" has a script that creates network plots, with a different package than ggplot2. It uses igraph,
network and visNetwork, among others. It creates 3 types of plots. 